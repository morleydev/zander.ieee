name := "Zander_Client_Test_Doc_Gen"

version := "prealpha-0.0.1"

scalaVersion := "2.10.3"

mainClass in (Compile, run) := Some("uk.co.morleydev.zander.ieee.Main")

libraryDependencies += "me.lessis" %% "hubcat" % "0.1.1"
