package uk.co.morleydev.zander.ieee

import scala.io.Source

object Main {

  def using[A, B <: {def close() : Unit}](closeable: B)(f: B => A): A = {
    try {
      f(closeable)
    } finally {
      closeable.close()
    }
  }

  def logTestRun(author : String, testClass : String, given : String,when : String, then : String) {
    println("-----------------------------------------------")
    println("Class/System Under Test: %s".format(testClass))
    println("Approval Author: %s".format(author))
    println("Test: %s %s".format(given, when))
    println("Pass Criteria: %s".format(then))
    println("-----------------------------------------------")
    println()
  }

  def main(args: Array[String]) {

    if (args(0) == "client") {
      processClientTests(args(1))
    } else {
      processServerTests(args(1))
    }
  }

  def processClientTests(filename: String) {
    println("===============================================")
    println("Zander Client IEEE 829-style Test Documentation")
    println("===============================================")
    println()

    var currentTestClass: String = ""
    var currentTestGiven: String = ""
    var currentTestWhen: String = ""

    using(Source.fromFile(filename)) {
      source => source.getLines()
        .filter(_.contains("[info]"))
        .map(s => s.substring("[info]".length))
        .map(s => s.replace("  ", ""))
        .dropWhile(!_.matches(".*([A-Z][a-z][0-9])*Tests:"))
        .takeWhile(!_.contains("Passed: :"))
        .foreach(s => {
        if (s.matches(".*([A-Z][a-z][0-9])*Tests:")) {
          currentTestClass = s.replace("Tests:", "")
          currentTestGiven = ""
          currentTestWhen = ""
        } else if (s.contains("Given")) {
          currentTestGiven = s
          currentTestWhen = ""
        } else if (s.contains("When")) {
          currentTestWhen = s
        } else {
          logTestRun("Jason Morley (morleydev.co.uk@gmail.com)",
            currentTestClass,
            currentTestGiven,
            currentTestWhen,
            s.replace(" - ", ""))
        }
      })
    }
  }

  def processServerTests(filename: String) {
    println("===============================================")
    println("Zander Server IEEE 829-style Test Documentation")
    println("===============================================")
    println()

    val currentTestClass: String = "Server Rest API"
    var currentTestGiven: String = ""
    var currentTestWhen: String = ""

    using(Source.fromFile(filename)) {
      source => source.getLines()
        .map(s => s.replace("  ", ""))
        .foreach(s => {
        if (s.contains("Given")) {
          currentTestGiven = s
          currentTestWhen = ""
        } else if (s.contains("When")) {
          currentTestWhen = s
        } else if (s.contains("Then")) {
          logTestRun("Jason Morley (morleydev.co.uk@gmail.com)",
            currentTestClass.replace("\u001B[0m", ""),
            currentTestGiven.replace("\u001B[0m", ""),
            currentTestWhen.replace("\u001B[0m", ""),
            s.replace("√", "")
              .replace("\u001B[0m", "")
              .replace("\u001B[32m","")
              .replace("\u001B[90m","")
              .drop(4))
        }
      })
    }
  }
}